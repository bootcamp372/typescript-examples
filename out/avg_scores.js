"use strict";
//let scores: Array<number> = [65,75,60,75,83,99,52,85];
let scores = [
    93, 86, 73, 79, 83, 100, 94
];
let total = sumNumbersInArray(scores);
let avg = total / scores.length;
console.log(`Average score: ${avg}`);
console.log(`Number of scores: ${scores.length}`);
scores.sort((n1, n2) => n1 - n2);
let highScore = getHighScore(scores);
let lowScore = getLowScore(scores);
console.log(`Low score: ${lowScore}`);
console.log(`High score: ${highScore}`);
// The displayStat function will concatenate the two
// values passed separated by a color and display
// the result
displayStat("Average", avg);
displayStat("High Score", highScore);
displayStat("Low Score", lowScore);
function sumNumbersInArray(scores) {
    let sum;
    sum = 0;
    for (let i = 0; i < scores.length; i++) {
        sum += scores[i];
    }
    return sum;
}
function getLowScore(scores) {
    return scores[0];
}
function getHighScore(scores) {
    return scores[scores.length - 1];
}
function displayStat(key, value) {
    console.log(`${key}, ${value}`);
}
