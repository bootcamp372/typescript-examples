"use strict";
//product-size-pickupDate
let order1;
let order2;
let order3;
order1 = 'HAM-L-1220';
order2 = 'TURKEY-M-1125';
order3 = 'BEEF-S-1118';
parseOrder(order1);
parseOrder(order2);
parseOrder(order3);
function parseOrder(product) {
    let meat;
    let meatCapitalized;
    let size;
    let date;
    let dateFormatted;
    let stringArr;
    stringArr = product.split('-');
    meat = stringArr[0];
    size = stringArr[1];
    date = stringArr[2];
    switch (size) {
        case "S":
            size = "Small";
            break;
        case "M":
            size = "Medium";
            break;
        case "L":
            size = "Large";
            break;
    }
    meatCapitalized = capitalize(meat);
    dateFormatted = addDash(date);
    console.log("meat : " + meatCapitalized);
    console.log("size : " + size);
    console.log("date : " + dateFormatted);
    console.log(`${size} ${meatCapitalized}  picked up on: ${dateFormatted}.`);
}
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
function addDash(string) {
    var chars = string.slice(0, 2) + "-" + string.slice(2);
    return chars;
}
