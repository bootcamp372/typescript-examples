//let scores: Array<number> = [65,75,60,75,83,99,52,85];
var scores = [
    93, 86, 73, 79, 83, 100, 94
];
var total = sumNumbersInArray(scores);
var avg = total / scores.length;
console.log("Average score: ".concat(avg));
console.log("Number of scores: ".concat(scores.length));
scores.sort(function (n1, n2) { return n1 - n2; });
var highScore = getHighScore(scores);
var lowScore = getLowScore(scores);
console.log("Low score: ".concat(lowScore));
console.log("High score: ".concat(highScore));
// The displayStat function will concatenate the two
// values passed separated by a color and display
// the result
displayStat("Average", avg);
displayStat("High Score", highScore);
displayStat("Low Score", lowScore);
function sumNumbersInArray(scores) {
    var sum;
    sum = 0;
    for (var i = 0; i < scores.length; i++) {
        sum += scores[i];
    }
    return sum;
}
function getLowScore(scores) {
    return scores[0];
}
function getHighScore(scores) {
    return scores[scores.length - 1];
}
function displayStat(key, value) {
    console.log("".concat(key, ", ").concat(value));
}
