// create an Employee class. It should define the following properties for an
// employee:
// - id
// - name
// - jobTitle
// - payRate (per hour)

class Employee {
    private empid: number;
    public name: string;
    public jobTitle: string;
    public payRate: number;

    constructor(empid: number, name: string, jobTitle: string, payRate: number) {
        this.empid = empid;
        this.name = name;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }

    toString(): string {
        return `${this.name} is a ${this.jobTitle} earning $${this.payRate} per hour.`;
    }
}

let employee1 = new Employee(1, "Earl E. Bird", "Baker", 20);
console.log( employee1.toString() );

let employee2 = new Employee(1, "Paige Turner", "Author", 25);
console.log( employee2.toString() );