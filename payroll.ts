// Create a script named payroll.ts that starts with 3 variables defined with values:
// payRate, hoursWorked, and filingStatus ("Single" or "Joint").

let payRate: number = 20;
let hoursWorked: number = 45;
let filingStatus: string = "Single";
let weeklyGrossPay: number;
let annualGrossPay: number;

// Start by calculating weeklyGrossPay. Pay 1.5x for hours worked over 40.
console.log(`Pay Rate: ${payRate}`);
console.log(`-----------------------------------`);

if (hoursWorked > 40) {
    let overtime: number = hoursWorked - 40;
    //hoursWorked += overtime * 1.5;
    console.log(`Hours Worked: ${hoursWorked}`);
    console.log(`Overtime: ${overtime}`);
    console.log(`-----------------------------------`);
    weeklyGrossPay = payRate * 40;
    console.log(`Weekly Gross Pay before OT: ${weeklyGrossPay}`);
    weeklyGrossPay += overtime * (payRate * 1.5);
    console.log(`Weekly Gross Pay after OT: ${weeklyGrossPay}`);
} else {
    weeklyGrossPay = payRate * hoursWorked;
    console.log(`Weekly Gross Pay: ${weeklyGrossPay}`);
    console.log(`Hours Worked: ${hoursWorked}`);
    console.log(`-----------------------------------`);
}

// Then, use that number to estimate annual gross pay by multiplying by 52. Save it in a
// new variable.

annualGrossPay = weeklyGrossPay * 52;
console.log(`Annual Gross Pay: ${annualGrossPay}`);
console.log(`-----------------------------------`);


// Determine tax rate
// The tax table for single
// filers is:
// The tax table for joint filers is:

console.log(`Filing Status: ${filingStatus}`);
console.log(`-----------------------------------`);

let taxRate: number;

if (filingStatus === "Single") {
    if (annualGrossPay < 23000) {
        taxRate = 0.05;
    } else if ((23000 < annualGrossPay) && (annualGrossPay <= 74999.99)) {
        taxRate = 0.12;
        console.log(`Got here: ${taxRate}`);
    } else if (annualGrossPay <= 75000) {
        taxRate = 0.2;
    } else {
        taxRate = 0;
    }
    getNetPay(taxRate);
}


if (filingStatus === "Joint") {
    if (annualGrossPay < 23000) {
        taxRate = 0;
    } else if ((23000 < annualGrossPay) && (annualGrossPay <= 74999.99)) {
        taxRate = 0.9;
        console.log(`Got here: ${taxRate}`);
    } else if (annualGrossPay <= 75000) {
        taxRate = 0.2;
    } else {
        taxRate = 0;
    }
    getNetPay(taxRate);
}



function getNetPay(taxRate: number) {
    let taxWitholdings: number;
    console.log(`Tax rate: ${taxRate}`);
    console.log(`annualGrossPay: ${annualGrossPay}`);
    taxWitholdings = annualGrossPay * taxRate;
    console.log(`Tax Witholdings: ${taxWitholdings}`);
    console.log(`-----------------------------------`);

    let netAnnualPay: number;
    netAnnualPay = annualGrossPay - taxWitholdings;
    console.log(`Annual Net Pay: ${netAnnualPay}`);
    console.log(`-----------------------------------`);

    // Now display the results of your program. Output should resemble:
    // You worked 45 hours this period.
    // Because you earn $10.00 per hour, your gross pay is $475.00
    // Your filing status is Single
    // Your tax withholdings this period is $47.50
    // Your net pay is $427.50
    console.log(
        `You worked ${hoursWorked} hours this period.`
    );
    console.log(
        `Because you earn $${payRate} per hour, your gross pay is $${annualGrossPay}.`
    );
    console.log(
        `Your filing status is ${filingStatus}.`
    );
    console.log(
        `Your tax withholdings this period are $${taxWitholdings}.`
    );
    console.log(
        `Your net pay is $${netAnnualPay}.`
    );
}

