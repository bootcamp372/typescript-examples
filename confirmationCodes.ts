//product-size-pickupDate

let order1: string;
let order2: string;
let order3: string;

order1 = 'HAM-L-1220';
order2 = 'TURKEY-M-1125';
order3 = 'BEEF-S-1118';

parseOrder(order1);
parseOrder(order2);
parseOrder(order3);

function parseOrder(product: string) {

    let meat: string;
    let meatCapitalized: string;
    let size: string;
    let date: string;
    let dateFormatted: string;
    let stringArr: string[];

    stringArr = product.split('-');

    meat = stringArr[0];
    size = stringArr[1];
    date = stringArr[2];

    switch (size) {
        case "S":
            size = "Small";
            break;
        case "M":
            size = "Medium";
            break;
        case "L":
            size = "Large";
            break;
    }

    meatCapitalized = capitalize(meat);
    dateFormatted = addDash(date);

    console.log("meat : " + meatCapitalized);
    console.log("size : " + size);
    console.log("date : " + dateFormatted);
    console.log(`${size} ${meatCapitalized}  picked up on: ${dateFormatted}.`);
}

function capitalize(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function addDash(string: string){
    var chars = string.slice(0, 2) + "-" + string.slice(2);
    return chars;
}
